from helpers import ReadFromFile, GetPrefixSuffix
from collections import defaultdict

READ_PATH = "Problems/rosalind_ba3e.txt"
WRITE_PATH = "Answers/rosalind_ba3e_ans.txt"

def DeBruijnFromKmers(Patterns):
	d = defaultdict(list)
	kmers = []
	for Pattern in Patterns:
		kmers.append((Pattern, GetPrefixSuffix(Pattern)))
	for kmer, vals in kmers:
		d[vals["prefix"]].append(vals["suffix"])
	return d

patterns = ReadFromFile(READ_PATH)
graph = DeBruijnFromKmers(patterns)

# Write the answer to the path
f = open(WRITE_PATH,'w')

for key, val in sorted(graph.items()):
	token = key + " -> "
	valx = sorted(val)
	for i in xrange(0, len(valx)):
		if len(val) == 1:
			token = token + valx[i]
		elif len(val) - 1 == i:
			token = token + valx[i]
		else:
			token = token + valx[i] + ","

	f.write(token + "\n")
f.close