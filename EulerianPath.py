from helpers import ReadFromFile, GetPrefixSuffix, WriteToFile, ConstructGraph, EulerianCycle

READ_PATH = "Problems/rosalind_ba3g.txt"
WRITE_PATH = "Answers/rosalind_ba3g_ans.txt"

def EulerianPath(Graph):
	# Get a list of all values in the graph
	outer_nodes = []
	for item in Graph.values():
		outer_nodes+=item
	# iterate through every single node in graph
	# to find nodes with unbalanced connections
	for node in set(outer_nodes + Graph.keys()):
		in_degree = outer_nodes.count(node)
		if node in Graph:
			out_degree = len(Graph[node]) 
		else:
			out_degree = 0
		# label the node the graphs starts from
		# and ends at
		if in_degree < out_degree:
			unbalanced_from = node
		if out_degree < in_degree:
			unbalanced_to = node
	Graph[unbalanced_to].append(unbalanced_from)
	# get the eulerian cycle to be edited to eulerian path
	eulerian_cycle = EulerianCycle(Graph)
	# find where the unbalanced_from node connects to the unbalanced_to node
	# in the cycle
	divide_point = filter(lambda i: eulerian_cycle[i:i+2] == [unbalanced_to, unbalanced_from], xrange(len(eulerian_cycle)-1))[0]
	# tear appart the cycle from where after and before unbalanced_to and unbalanced_from
	# node connect to each other
	# also start the beginning part from the beginning of the part + 1
	return eulerian_cycle[divide_point+1:] + eulerian_cycle[1:divide_point+1]

lines = ReadFromFile(READ_PATH)
graph = ConstructGraph(lines)
path = EulerianPath(graph)

f = open(WRITE_PATH, "w")
token = ""
for i in xrange(0, len(path)):
	if i == len(path) - 1:
		token = token + str(path[i])
	else:
		token = token + str(path[i]) + "->"
f.write(token)
