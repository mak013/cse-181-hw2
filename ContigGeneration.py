from helpers import ReadFromFile
from collections import defaultdict
from compiler.ast import flatten

READ_PATH = "Problems/rosalind_ba3k.txt"
WRITE_PATH = "Answers/rosalind_ba3k_ans.txt"

def ContigGeneration(Patterns):
	# construct the graph
	graph = defaultdict(list)
	for Pattern in Patterns:
		if Pattern[:-1] in graph:
			graph[Pattern[:-1]].append(Pattern[1:])
		else:
			graph[Pattern[:-1]] = [Pattern[1:]]
	# Calculate the indegree and outdegree of nodes
	balanced = []
	non_balanced =[]

	outer_nodes = reduce(lambda a,b: a + b, graph.values())
	for node in set(outer_nodes + graph.keys()):
		out_degree = outer_nodes.count(node)
		if node in graph:
			in_degree = len(graph[node])
		else:
			in_degree = 0
        # Check if it counts as a branch
        if in_degree == out_degree == 1:
            balanced.append(node)
        else:
            non_balanced.append(node)

	get_contigs = lambda s, c: flatten([c+e[-1] if e not in balanced else get_contigs(e,c+e[-1]) for e in graph[s]])
	contigs = sorted(flatten([get_contigs(start,start) for start in set(non_balanced) & set(graph.keys())]))

	print contigs

kmers = ReadFromFile(READ_PATH)
ContigGeneration(kmers)
