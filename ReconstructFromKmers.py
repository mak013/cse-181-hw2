from helpers import ReadFromFile, EulerianPath
from collections import defaultdict

READ_PATH = "Problems/rosalind_ba3h.txt"
WRITE_PATH = "Answers/rosalind_ba3h_ans.txt"

def ReconstructFromKmers(k, Patterns):
	# Create a graph from Patterns
	graph = defaultdict(list)
	for Pattern in Patterns:
		if Pattern[:-1] in graph:
			graph[Pattern[:-1]].append(Pattern[1:])
		else:
			graph[Pattern[:-1]] = [Pattern[1:]]
	# Get the eulerian path
	path = EulerianPath(graph)
	string = path[0] + ''.join(map(lambda x: x[-1], path[1:]))
	print string
	return string

lines = ReadFromFile(READ_PATH)
patterns = lines[1:len(lines)]
k = int(lines[0])
path = ReconstructFromKmers(k, patterns)

f = open(WRITE_PATH, "w")
f.write(path[0] + "\n")
f.close()