from helpers import ReadFromFile, WriteToFile

def ReconstructString(Patterns):
	k = len(Patterns[0])
	genome = Patterns[0]
	for i in xrange(1, len(Patterns)):
		genome += Patterns[i][k-1:k]
	return genome

Patterns = ReadFromFile("Problems/rosalind_ba3b.txt")
WriteToFile([ReconstructString(Patterns)], "Answers/rosalind_ba3b_ans.txt")