from helpers import AllKmers, GetPrefixSuffix, ReadFromFile
from collections import defaultdict

READ_PATH = "Problems/rosalind_ba3d.txt"
WRITE_PATH = "Answers/rosalind_ba3d_ans.txt"

def DeBruijn(k, Text):
	d = defaultdict(list)
	for i in xrange(len(Text) - k + 1):
		d[Text[i:i+k-1]].append(Text[i+1:i+k])
	return d

lines = ReadFromFile(READ_PATH)
graph = DeBruijn(int(lines[0]), lines[1])

f = open(WRITE_PATH,'w')

for key, val in sorted(graph.items()):
	token = key + " -> "
	for i in xrange(0, len(val)):
		if len(val) == 1:
			token = token + val[i]
		elif len(val) - 1 == i:
			token = token + val[i]
		else:
			token = token + val[i] + ","

	f.write(token + "\n")
f.close()