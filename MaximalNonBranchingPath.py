from helpers import ReadFromFile
from collections import defaultdict

READ_PATH = "Problems/rosalind_ba3m.txt"
WRITE_PATH = "Answers/rosalind_ba3m_ans.txt"

def MaximalNonBranchingPath(EdgeSets):
	path = []

	# build the graph from the lines of the file
	graph = defaultdict(list)
	for EdgeSet in EdgeSets:
		data = EdgeSet.split()
		node = data[0]
		edges = data[2].split(',')
		if node in graph:
			graph[node].append(edges.items())
		else:
			graph[node] = edges
	# calcualte the out_degree and in_degree of each
	# and every node
	out_nodes = reduce(lambda a,b: a + b, graph.values())
	node_degree = {}
	for node in set(out_nodes + graph.keys()):
		in_degree = out_nodes.count(node)
		if node in graph:
			out_degree = len(graph[node])
		else:
			out_degree = 0
		node_degree[node] = {
			"in_degree": in_degree,
			"out_degree": out_degree
		}
	# find the non-maximal branch
	for node in set(out_nodes + graph.keys()):
		if node_degree[node]["in_degree"] != node_degree[node]["out_degree"]:
			if node_degree[node]["out_degree"] > 0:
				for edge_node in graph[node]:
					non_branching_path = [node, edge_node]
					w = edge_node
					while node_degree[w]["in_degree"] == 1 and node_degree[w]["out_degree"] == 1:
						non_branching_path.append(graph[w][0])
						w = graph[w][0]
					path.append(non_branching_path)
	# handle isolated cycles 
	already_isolated = []
	for node in set(out_nodes + graph.keys()):
		if node_degree[node]["in_degree"] == 1 and node_degree[node]["out_degree"] == 1:
			if node in already_isolated:
				continue
			x = graph[node][0]
			is_cycle = False
			while node_degree[x]["in_degree"] == 1 and node_degree[x]["out_degree"] == 1:
				x = graph[x][0]
				if x == node:
					is_cycle = True
					break
			if is_cycle:
				isolated_cycle = [node]
				x = graph[node][0]
				while node_degree[x]["in_degree"] == 1 and node_degree[x]["out_degree"] == 1:
					isolated_cycle.append(x)
					if x == node:
						break
					x = graph[x][0]
				path.append(isolated_cycle)
				already_isolated = isolated_cycle + already_isolated
	return path

lines = ReadFromFile(READ_PATH)
branches = MaximalNonBranchingPath(lines)

f = open(WRITE_PATH, "w")
for branch in branches:
	token = ""
	for i in xrange(len(branch)):
		if i == len(branch) - 1:
			token = token + branch[i]
		else:
			token = token + branch[i] + " -> "
	f.write(token + "\n")
f.close()