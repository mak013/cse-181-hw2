from helpers import ReadFromFile, GetPrefixSuffix, WriteToFile
from collections import defaultdict
from random import randint

READ_PATH = "Problems/rosalind_ba3f.txt"
WRITE_PATH = "Answers/rosalind_ba3f_ans.txt"

# Construct a graph from the file
def ConstructGraph(Lines):
	g = defaultdict(list)
	for Line in Lines:
		node = int(Line.split(" -> ")[0])
		edges = Line.split(" -> ")[1].split(",")
		for edge in edges:
			g[node].append(int(edge))
	return g
# Find the eulerian path
def EulerianCycle(Graph):
	# create an initial cycle
	current = Graph.keys()[0]
	eulerian_cycle = [current]
	while True:
		eulerian_cycle.append(Graph[current][0])
		if len(Graph[current]) == 1:
			del Graph[current]
		else:
			Graph[current] = Graph[current][1:]
		# get the last node at the end of the cycle
		if eulerian_cycle[-1] in Graph:
			current = eulerian_cycle[-1]
		else:
			break
	# explore all the other edges of the graph
	while (len(Graph) > 0):
		for i in xrange(0, len(eulerian_cycle)):
			if eulerian_cycle[i] in Graph:
				current = eulerian_cycle[i]
				partial_cycle = [current]
				while True:
					partial_cycle.append(Graph[current][0])
					if len(Graph[current]) == 1:
						del Graph[current]
					else:
						Graph[current] = Graph[current][1:]
					# get the last node at the end of the cycle
					if partial_cycle[-1] in Graph:
						current = partial_cycle[-1]
					else:
						break
				eulerian_cycle = eulerian_cycle[:i] + partial_cycle + eulerian_cycle[i+1:]
	return eulerian_cycle
# construct the graph =
lines = ReadFromFile(READ_PATH)
graph = ConstructGraph(lines)
path = EulerianCycle(graph)

f = open(WRITE_PATH, "w")
token = ""
for i in xrange(0, len(path)):
	if i == len(path) - 1:
		token = token + str(path[i])
	else:
		token = token + str(path[i]) + "->"
f.write(token)
