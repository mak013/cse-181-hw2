''' 
	Note to self, becareful of termination character when submitting
	answer to rosalind
'''

from helpers import AllKmers, ReadFromFile, WriteToFile

def StringComposition(k, Text):
	kmers = AllKmers(Text, k)
	return list(sorted(kmers))

''' ************************************************ '''
# Read the input file and parse it to get the parameters 
lines = ReadFromFile("Problems/rosalind_ba3a.txt")
k = int(lines[0])
text = lines[1]
# Call the function 
compistion = StringComposition(k, text)
WriteToFile(compistion, "Answers/rosalind_ba3a_ans.txt")