from helpers import ReadFromFile

# File write path
file_name = "Answers/rosalind_ba3c_ans.txt"

''' 
	a kmer's adjacency list will be kmers with prefixes that matches it's suffix
'''
# construct the prefixes and suffixes of the kmers
def GetPrefixSuffix(kmer):
	k = len(kmer)
	prefix = kmer[0:k-1]
	suffix = kmer[1:k]
	return {
		"prefix": prefix,
		"suffix": suffix
	}
# construct our overlap using an adj list
def OverlapGraph(Kmers):
	overlapGraph = []
	# construct a list of kmers with their prefixses and suffixes
	kmers = []
	for Kmer in Kmers:
		kmers.append((Kmer, GetPrefixSuffix(Kmer)))
	# search for kmers with suffixes matching prefixes to 
	# create a graph
	for kmer, pre_suf in kmers:
		adj_list = []
		for kmerx, pre_sufx in kmers:
			if kmer == kmerx:
				continue
			if pre_suf["suffix"] == pre_sufx["prefix"]:
				adj_list.append(kmerx)
		overlapGraph.append((kmer, adj_list))
	return overlapGraph

kmers = ReadFromFile("Problems/rosalind_ba3c.txt")
graph = OverlapGraph(kmers)

# write our answers to the file
f = open(file_name, "w")
for kmer, adj_list in sorted(graph):
	token = kmer + " ->"
	if len(adj_list):
		for kmerx in adj_list:
			token = token + " " + kmerx
		f.write(token + "\n")
	else:
		continue
f.close()