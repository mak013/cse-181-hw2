from collections import defaultdict
from random import randint

# Find all the kmers in a dna string
def AllKmers(dna, k):
    return [dna[i:i + k] for i in xrange(len(dna) - k + 1)]
# Print out the list to a file
def WriteToFile(lists, file_name):
	f = open(file_name,'w')
	for item in lists:
		f.write(str(item) + "\n")
	f.close()
# Read out the parameters from a file
def ReadFromFile(File):
	f = open(File, "r")
	lines = [ln.rstrip('\n').strip() for ln in f]
	return [ln for ln in lines if ln]
# construct the prefixes and suffixes of the kmers
def GetPrefixSuffix(kmer):
	k = len(kmer)
	prefix = kmer[0:k-1]
	suffix = kmer[1:k]
	return {
		"prefix": prefix,
		"suffix": suffix
	}
# Construct a graph from the file
def ConstructGraph(Lines):
	g = defaultdict(list)
	for Line in Lines:
		node = int(Line.split(" -> ")[0])
		edges = Line.split(" -> ")[1].split(",")
		for edge in edges:
			g[node].append(int(edge))
	return g
# Find the eulerian path
def EulerianCycle(Graph):
	# create an initial cycle
	current = Graph.keys()[0]
	eulerian_cycle = [current]
	while True:
		eulerian_cycle.append(Graph[current][0])
		if len(Graph[current]) == 1:
			del Graph[current]
		else:
			Graph[current] = Graph[current][1:]
		# get the last node at the end of the cycle
		if eulerian_cycle[-1] in Graph:
			current = eulerian_cycle[-1]
		else:
			break
	# explore all the other edges of the graph
	while (len(Graph) > 0):
		for i in xrange(0, len(eulerian_cycle)):
			if eulerian_cycle[i] in Graph:
				current = eulerian_cycle[i]
				partial_cycle = [current]
				while True:
					partial_cycle.append(Graph[current][0])
					if len(Graph[current]) == 1:
						del Graph[current]
					else:
						Graph[current] = Graph[current][1:]
					# get the last node at the end of the cycle
					if partial_cycle[-1] in Graph:
						current = partial_cycle[-1]
					else:
						break
				eulerian_cycle = eulerian_cycle[:i] + partial_cycle + eulerian_cycle[i+1:]
	return eulerian_cycle
# Get the Eulerian path from a graph
def EulerianPath(Graph):
	# Get a list of all values in the graph
	outer_nodes = []
	for item in Graph.values():
		outer_nodes+=item
	# iterate through every single node in graph
	# to find nodes with unbalanced connections
	for node in set(outer_nodes + Graph.keys()):
		in_degree = outer_nodes.count(node)
		if node in Graph:
			out_degree = len(Graph[node]) 
		else:
			out_degree = 0
		# label the node the graphs starts from
		# and ends at
		if in_degree < out_degree:
			unbalanced_from = node
		if out_degree < in_degree:
			unbalanced_to = node
	Graph[unbalanced_to].append(unbalanced_from)
	# get the eulerian cycle to be edited to eulerian path
	eulerian_cycle = EulerianCycle(Graph)
	# find where the unbalanced_from node connects to the unbalanced_to node
	# in the cycle
	divide_point = filter(lambda i: eulerian_cycle[i:i+2] == [unbalanced_to, unbalanced_from], xrange(len(eulerian_cycle)-1))[0]
	# tear appart the cycle from where after and before unbalanced_to and unbalanced_from
	# node connect to each other
	# also start the beginning part from the beginning of the part + 1
	return eulerian_cycle[divide_point+1:] + eulerian_cycle[1:divide_point+1]