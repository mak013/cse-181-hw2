from helpers import EulerianPath, ReadFromFile
from collections import defaultdict	

READ_PATH = "Problems/rosalind_ba3j.txt"
WRITE_PATH = "Answers/rosalind_ba3j_ans.txt"

def ReconstructFromReadPairs(k,d, PairedReads):
	# generate the read pairs
	pair_reads = []
	for PairRead in PairedReads:
		pair_reads.append((PairRead.split("|")[0], PairRead.split("|")[1]))
	# create di bruijn graph
	graph = defaultdict(list)
	for pair_read in pair_reads:
		if (pair_read[0][:-1],pair_read[1][:-1]) in graph:
			graph[(pair_read[0][:-1],pair_read[1][:-1])].append((pair_read[0][1:],pair_read[1][1:]))
		else:
			graph[(pair_read[0][:-1],pair_read[1][:-1])] = [(pair_read[0][1:],pair_read[1][1:])]
	# calculate the di bruijin graph
	path = EulerianPath(graph)
	# modify the di bruijin graph
	read_pair_string = []
	for i in xrange(2):
		lastChar = lambda x: x[i][-1]
		lastCharKmers = map(lastChar, path[1:])
		read_pair_string.append(path[0][i] + "".join(lastCharKmers))

	return read_pair_string[0][:k+d] + read_pair_string[1]

# parse the content of the file
lines = ReadFromFile(READ_PATH)
k = int(lines[0].split()[0])
d = int(lines[0].split()[1])
pair_reads = lines[1:]

string = ReconstructFromReadPairs(k,d,pair_reads)

f = open(WRITE_PATH, "w")
f.write(string)
f.close()