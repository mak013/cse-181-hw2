from collections import defaultdict
from helpers import EulerianCycle, ReadFromFile

READ_PATH = "Problems/rosalind_ba3i.txt"
WRITE_PATH = "Answers/rosalind_ba3i_ans.txt"

def UniversalString(k):
	# generate a list of the binary kmers
	bin_kmers = []
	for i in range(int(k*"1",2) + 1):
		construct_kmer = bin(i)[2:]
		construct_kmer = (k - len(construct_kmer))*"0" + construct_kmer
		bin_kmers.append(construct_kmer)

	# generate a binary kmer graph
	d = defaultdict(list)
	for bin_kmer in bin_kmers:
		if bin_kmer[:-1] in d:
			d[bin_kmer[:-1]].append(bin_kmer[1:])
		else:
			d[bin_kmer[:-1]] = [bin_kmer[1:]]
	# get the eulerian cycle and modify it into a 
	# universal string
	cycle = EulerianCycle(d)

	return ''.join([item[0] for item in cycle[:-1]])

k = int(ReadFromFile(READ_PATH)[0])
u_string = UniversalString(k)

f = open(WRITE_PATH, "w")
f.write(u_string)
f.close()